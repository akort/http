const express = require('express');
const app = express();
const http = require("http")
const url  = require("url")
const fs = require("fs")

let index = fs.readFileSync("index.html")
let test = fs.readFileSync("test.png")
let zb = fs.readFileSync("zb.png")
let add = fs.readFileSync("add.js")
let mainjs = fs.readFileSync("main.js")

let img;// = fs.readFileSync("img.png")


http.createServer(function (req,res) {
	let headers = {}
	let body;
	let status =200;

	let urls = {
		"/": function () {
			headers["Content-Type"] = "text/html"
			body = index
		},
		/*"/test.png": function () {
			headers["Content-Type"] = "image/png"
			body = test
		},*/
		"/zb.png":function () {
			headers["Content-Type"] = "image/png"
			body = zb
		},
		"/main.js": function () {
			headers["Content-Type"] = "application/javascript"
			body = mainjs
		},
		"/add.js":function (){
			headers["Content-Type"] = "application/javascript"
			body = add
		}
	}
	
	// testing with 100 images
	let i;
	for (i = 1; i<101;i++){
		let filename = "img"+i+".png"
		img=fs.readFileSync(filename) // we could use any of the images here
		urls["/"+filename] = function (){
			headers["Content-Type"] = "image/png"
			body = img
		}
	}
	i = 1;
		
	urls[req.url]()

	headers["Content-Length"] = body.length
	res.writeHead(status,headers)
	res.end(body)
}).listen(8000)
